// event driven communication has two part
// emitters==> event trigger here
// listeners ==> handler for event 

var events = require('events');
var myEvent = new events.EventEmitter();
var myEvent1 = new events.EventEmitter();

setTimeout(function(){
    myEvent.emit('kishor','hi');
},3000)

myEvent1.on('kishor',function(data){
    console.log('i am listener of event',data);
})