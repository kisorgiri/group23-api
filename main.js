// import the file operation
const fileOp = require('./file');

console.log('what comes out >>', fileOp);

fileOp.write('kishor.txt', 'i am good today')
    .then(function (data) {
        console.log('data is >>', data);
    })
    .catch(function (err) {
        console.log('error in write >>', err);
    })

fileOp.read('kishor.txt')
    .then(function (data) {
        console.log('data is >>', data);
    })
    .catch(function (err) {
        console.log('error in read >>', err);
    })