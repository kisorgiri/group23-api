const router = require('express').Router();
// / impprt application level middleware
const authenticate = require('./../middlewares/authenticate');
const authorize = require('./../middlewares/authorize');
// import routing level middleware
const authRouter = require('./../controllers/auth.route');
const userRouter = require('./../controllers/user.route');
const productRouter = require('./../controllers/product.route');

router.use('/auth', authRouter);
router.use('/user', authenticate, userRouter)
router.use('/product', productRouter);
router.use('/comment', authenticate, authorize, userRouter)
router.use('/notification', userRouter)
router.use('/message', userRouter)
router.use('/payment', userRouter)
module.exports = router;