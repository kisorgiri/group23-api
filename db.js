const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');

// mongodb://localhost:27017/db_name
mongoose.connect(dbConfig.conxnURL + '/' + dbConfig.dbName, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, done) {
    if (err) {
        console.log('error >>>', err);
    } else {
        console.log('db connection successful');
    }
});