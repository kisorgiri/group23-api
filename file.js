// file file communication
// module -module communication

// export 
// import

// if we want to import inbuilt module or moudle installed from npmjs
const fs = require('fs');

// // create file
function write(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(fileName, content, function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    })

}

// write('abc.txt', 'hi')
//     .then(function (data) {
//         console.log('success >>', data);
//     })
//     .catch(function (err) {
//         console.log('failure >>', err);
//     })
// write('xuz.txt', 'hello');
function read(fileName) {
    return new Promise(function (resolve, reject) {
        fs.readFile(fileName, function (err, done) {
            if (err) {
                reject(err);
            }
            else {
                resolve(done.toString())
            }
        });
    })

}
// make it functional
// return that function from this file
// fs.rename('./abc.txt', './new-abc.txt', function (err, done) {
//     if (err) {
//         console.log('rename filed >>', err);
//     } else {
//         console.log('rename success >>', done);
//     }
// })

// fs.unlink('./new-abc.txt', function (err, done) {
//     if (err) {
//         console.log('err is >>', err);
//     }
//     else {
//         console.log('remove success >', done);
//     }
// })




module.exports = {
    write,
    read
};
