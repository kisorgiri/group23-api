const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
// node_modules package and nodejs internal packages should be called without giving path
const config = require('./configs/index');
const app = express();
const path = require('path');

// db
require('./db');

// events
const events = require('events');
const myEvent = new events.EventEmitter();

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

// eg for error
myEvent.on('error', function (err, res) {
    console.log('error in my event is >>', err);
    res.json({
        msg: "from my own event driven communication",
        data: err
    });
})
// app.set('port',9090);

// Socketpart
const socket = require('socket.io');
const io = socket(app.listen(config.socketPort));
var users = [];
io.on('connection', function (client) {
    var id = client.id;
    console.log("client connected to socket server");
    client.on('new-msg', function (data) {
        console.log('new message is >>', data);
        client.emit('reply-msg-own', data); // emit only send response to requester client
        // to send message to all connected client we have to use broadcast
        client.broadcast.to(data.receiverId).emit('reply-msg', data);
    })
    client.on('new-user', function (username) {
        users.push({
            name: username,
            id
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    });
    client.on('disconnect', function () {
        users.forEach(function (item, i) {
            if (item.id === id) {
                users.splice(i, 1);
            }
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })
})

// load api routes
const apiRouter = require('./routes/api.route');
const { Socket } = require('dgram');
// inbuilt middleware
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
// this middleware parse incoming data with content-type x-www-formurlecoded and set data in the body
app.use(express.static('uploads')); // serving static file without path is done for internal purpose
console.log('__dir name outside >>', __dirname);
app.use('/file', express.static(path.join(__dirname, 'uploads'))); // path is given to serve the files to client

// load third party middleware
app.use(morgan('dev'));
// load routing level middleware

app.use(cors());

app.use('/api', apiRouter);

app.use(function (req, res, next) {
    console.log('here at 1st application level middleware');
    next({
        msg: 'Page Not Found',
        status: 404
    });
});

// error handling middleware middleware function that has 4 arguments 
// error handling middleware doesnot came into action in req-res cycle
// the first argument will be for error block followed by http req,res, and next
// to execute/call error handling middleware we must call next with arguments
// next with arguments is used to call error handling middleware
app.use(function (err, req, res, next) {
    console.log('error handling level middleware', err)
    // this is the middleware handling all the errors of the application
    res
        .status(err.status || 400)
        .json({
            msg: err.msg || err,
            status: err.status || 400
        });
});



app.listen(config.port, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + config.port);
    }
});

// middleware 
// middleware is a function that has access to http request object http response object
// and next middleware function
// middleware can access and modify request and response object

// the order of middleware is very important
// syntax
// function(req,res,next){

// }
// configuration block
// app.use(function(req,res,next){

// })

// types of middleware
// 1 application level middleware
// 2 routing level middleware
// 3 third party middleware
// 4 inbuilt middleware
// 5 error handling middleware

// nodejs globals
// __dirname ==> it gives the directory path
// __filename ==> it gives the filename of working file
// process
// global