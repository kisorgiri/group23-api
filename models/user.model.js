// db modelling
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// plugins

const UserSchema = new Schema({
    // data modelling
    name: String,
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true,
    },
    phoneNumber: {
        type: Number
    },
    dob: {
        type: Date
    },
    role: {
        type: Number, //1 for admin,2 for normal user,3 visitors
        default: 2
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    address: {
        temp_addr: [String],
        permanent_addr: String
    },
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    passwordResetExpiry: String,
    passwordResetToken: String
}, {
    timestamps: true
});

var UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;
