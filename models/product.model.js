const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const reviewSchema = new Schema({
    point: Number,
    message: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    // if review schema was independant
    // product:{
    //     type:Schema.Types.ObjectId,
    //     ref:'product'
    // }
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    // db modelling
    name: String,
    description: String,
    price: Number,
    modelNo: String,
    color: String,
    brand: String,
    category: String,
    size: String,
    manuDate: Date,
    expiryDate: Date,
    weight: String,
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
        },
        discountValue: String
    },
    status: {
        type: String,
        enum: ['available', 'out of stock'],
        default: 'available'
    },
    images: [String],
    tags: [String],
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    reviews: [reviewSchema]
}, {
    timestamps: true
});

const ProductModel = mongoose.model('product', ProductSchema);

module.exports = ProductModel;