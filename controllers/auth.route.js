const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/mapUserReq');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');
const randomString = require('randomstring');

const sender = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
});

function prepareMail(data) {
    let mailBody = {
        from: '"Group 23  Store" <noreply@group23store.com>', // sender address
        to: `${data.email},arkhatri101@gmail.com`, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `
        <p>Hello <strong>${data.name}</strong></p>
        <p>We noticed that you are having trouble logging into our system.
        plase use the link below to reset your password</p>
        <p>
        <a href="${data.link}">click here to reset your password</a>
        </p>
        <p>If you have not requested to change your password kindly ignore this email</p>
        <p>Regards,</p>
        <p>Group23 Support Team</p>`, // html body
    }
    return mailBody;
}

// routing level middleware

function generateToken(data) {
    var token = jwt.sign({ name: data.username, role: data.role, _id: data._id }, config.jwtSecret);
    return token;
}

router.get('/login', function (req, res, next) {
    //  render to template file
    res.render('login.pug', {
        title: 'Express',
        message: 'Welcome to Nodejs and Express'
    })
})



router.post('/login', function (req, res, next) {
    UserModel.findOne(
        {
            $or: [{
                username: req.body.username
            }, {
                email: req.body.username
            }]
        }
    )
        .then(function (user) {
            if (user) {
                const isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {

                    res.status(200).json({
                        user,
                        token: generateToken(user)
                    });
                }
                else {
                    next({
                        msg: "Invalid Password",
                        status: 400
                    })
                }

            } else {
                next({
                    msg: "Invalid Username",
                    status: 400
                });
            }
        })
        .catch(function (err) {
            next(err);
        })
})

router.get('/register', function (req, res, next) {
    //  render to template file
    res.render('register.pug');
})

router.post('/register', function (req, res, next) {

    console.log('req.body >>', req.body);
    const newUser = new UserModel({});
    // new user is mongoose object
    const newMappedUser = MapUser(newUser, req.body);
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })
})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: 'User not found with provided email'
                })
            }
            var passwordResetExpiry = new Date(Date.now() + 1000 * 60 * 60 * 2);
            var passwordResetToken = randomString.generate(25);
            // now send mail
            var data = {
                email: user.email,
                name: user.username,
                link: `${req.headers.origin}/reset-password/${passwordResetToken}`
            }

            user.passwordResetExpiry = passwordResetExpiry;
            user.passwordResetToken = passwordResetToken
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(prepareMail(data), function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                })
            })
        })
})

router.post('/reset-password/:token', function (req, res, next) {
    UserModel.findOne({
        passwordResetToken: req.params.token,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Invalid or expired token"
                })
            }
            // check validity
            // if (new Date(user.passwordResetExpiry).getTime() < Date.now()) {
            //     return next({
            //         msg: "Password reset link expired"
            //     })
            // }
            user.password = passwordHash.generate(req.body.password);
            // destroy the passwordresetToken
            user.passwordResetToken = null;
            user.passwordResetExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})


module.exports = router;