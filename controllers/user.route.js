const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUser = require('./../helpers/mapUserReq');
router.route('/')
    .get(function (req, res, next) {
        // res.send('from empty reques of user');
        UserModel
            .find({})
            .sort({
                _id: -1
            })
            // .skip(2)
            // .limit(2)
            .exec(function (err, result) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(result);
            })
    })
    .post(function (req, res, next) {
        console.log('here at post request of user >>', req.body);
        res.send('from user post');
    });
router.get('/dashboard', function (req, res, next) {
    console.log('req.myEvent >>', req.myEvent);
    require('fs').readFile('sdlkjf', function (err, done) {
        if (err) {
            return req.myEvent.emit('error', err, res);
        }
    })
})
// place your dynamic request handler at bottom
router.route('/:id')
    .get(function (req, res, next) {
        // UserModel.findOne({
        //     _id: req.params.id
        // })
        //     .exec(function (err, result) {
        //         if (err) {
        //             return next(err);
        //         }
        //         res.status(200).json(result);
        //     })
        UserModel.findById(req.params.id, function (err, result) {
            if (err) {
                return next(err);
            }
            res.status(200).json(result);
        })
    })

    .put(function (req, res, next) {
        UserModel.findById(req.params.id)
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    const updatedUser = MapUser(user, req.body);
                    updatedUser.updatedBy = req.loggedInUser.name;
                    updatedUser.save(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.status(200).json(done);
                    })

                }
                else {
                    next({
                        msg: 'User Not found',
                        status: 404
                    })
                }
            })
    })
    .delete(function (req, res, next) {
        UserModel.findById(req.params.id)
            .then(function (user) {
                if (user) {
                    user.remove(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.status(200).json(done);
                    })
                }
                else {
                    next({
                        msg: "user not found",
                        status: 404
                    })
                }
            })
            .catch(function (err) {
                next(err);
            });
    });

module.exports = router;