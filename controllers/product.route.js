const router = require('express').Router();
const ProductModel = require('./../models/product.model');
const isAdmin = require('./../middlewares/isAdmin');
const authenticate = require('./../middlewares/authenticate');
const multer = require('multer');

// basic usage
// const upload = multer({
//     dest: './uploads'
// });
function fileFilter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType === 'image') {
        cb(null, true);
    } else {
        req.fileErr = true;
        cb(null, false);
    }

}
var diskStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, './uploads/images');
    }
});
const upload = multer({
    storage: diskStorage,
    fileFilter: fileFilter
});

function map_product_req(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.weight)
        product.weight = productDetails.weight;
    if (productDetails.size)
        product.size = productDetails.size;
    if (productDetails.status)
        product.status = productDetails.status;
    if (productDetails.images)
        product.images = productDetails.images;
    if (productDetails.tags)
        product.tags = typeof (productDetails.tags) === 'string' ? productDetails.tags.split(',') : productDetails.tags;
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate;
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate;
    if (productDetails.modelNo)
        product.modelNo = productDetails.modelNo;
    if (productDetails.user) {
        product.user = productDetails.user;
    }
    if (productDetails.discountedItem || productDetails.discountType || productDetails.discountValue) {
        if (!product.discount) {
            product.discount = {};
        }
        product.discount.discountedItem = productDetails.discountedItem;
        if (productDetails.discountType)
            product.discount.discountType = productDetails.discountType;
        if (productDetails.discountValue)
            product.discount.discountValue = productDetails.discountValue;
    }
    if (productDetails.reviewPoint && productDetails.reviewMsg) {
        let review = {
            point: productDetails.reviewPoint,
            message: productDetails.reviewMsg,
            user: productDetails.user,

        }
        product.reviews.push(review);
    }
    return product;
}

router.route('/')
    .get(authenticate, function (req, res, next) {
        // fetch all product
        let condition = {};
        if (req.loggedInUser.role !== 1) {
            condition.user = req.loggedInUser._id;
        }
        //server side pagination
        let currentPage = Number(req.query.pageNumber) || 1;
        let pageCount = Number(req.query.pageSize) || 100;
        let skipCount = (currentPage - 1) * pageCount;
        // projection 
        // {inclusion 1 and exclusion 0}
        ProductModel
            .find(condition)
            .populate('user', { username: 1 })
            .skip(skipCount)
            .limit(pageCount)
            .exec(function (err, result) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(result);
            })


    })
    .post(authenticate, upload.single('img'), function (req, res, next) {
        if (req.fileErr) {
            return next({
                msg: "Invalid File Format",
                status: 400
            });
        }

        // insert product

        if (req.file) {
            req.body.images = [req.file.filename];
        }
        const newProduct = new ProductModel({});
        req.body.user = req.loggedInUser._id;
        map_product_req(newProduct, req.body);
        newProduct.save(function (err, done) {
            if (err) {
                return next(err);
            }
            res.status(200).json(done);
        })

    });


router.route('/search')
    .get(function (req, res, next) {
        var condition = {};
        var searchCondition = map_product_req(condition, req.query);
        if (req.body.minPrice) {
            searchCondition.price = {
                $gte: req.body.minPrice
            }
        }
        if (req.body.maxPrice) {
            searchCondition.price = {
                $lte: req.body.maxPrice
            }
        }
        if (req.body.minPrice && req.body.maxPrice) {
            searchCondition.price = {
                $gte: req.body.minPrice,
                $lte: req.body.maxPrice
            }
        }
        if (req.body.fromDate && req.body.toDate) {
            var fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0); // miliseconds
            var toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999); // miliseconds
            searchCondition.createdAt = {
                $gte: new Date(fromDate),
                $lte: new Date(toDate)
            }
        }
        if (req.body.tags) {
            searchCondition.tags = {
                $in: req.body.tags.split(',')
            }
        }
        ProductModel.find(searchCondition)
            .then(function (data) {
                res.status(200).json(data);
            })
            .catch(err => next(err));
    })
    .post(function (req, res, next) {
        var condition = {};
        var searchCondition = map_product_req(condition, req.body);
        if (req.body.minPrice) {
            searchCondition.price = {
                $gte: req.body.minPrice
            }
        }
        if (req.body.maxPrice) {
            searchCondition.price = {
                $lte: req.body.maxPrice
            }
        }
        if (req.body.minPrice && req.body.maxPrice) {
            searchCondition.price = {
                $gte: req.body.minPrice,
                $lte: req.body.maxPrice
            }
        }
        if (req.body.fromDate && req.body.toDate) {
            var fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0); // miliseconds
            var toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999); // miliseconds
            searchCondition.createdAt = {
                $gte: new Date(fromDate),
                $lte: new Date(toDate)
            }
        }
        if (req.body.tags) {
            searchCondition.tags = {
                $in: req.body.tags.split(',')
            }
        }
        ProductModel.find(searchCondition)
            .then(function (data) {
                res.status(200).json(data);
            })
            .catch(err => next(err));
    });

router.route('/:id')
    .get(authenticate, function (req, res, next) {
        ProductModel.findById(req.params.id).exec(function (err, product) {
            if (err) {
                return next(err);
            }
            if (product) {
                res.status(200).json(product);
            } else {
                next({
                    msg: "product not found",
                    status: 404
                })
            }
        })
    })
    .put(authenticate, upload.single('img'), function (req, res, next) {
        console.log('req.body. is >', req.body);
        console.log('req.file. is >', req.file);
        if (req.fileErr) {
            return next({
                msg: "Invalid File Format",
                status: 400
            });
        }

        // insert product
        ProductModel.findById(req.params.id)
            .exec(function (err, product) {
                if (err) {
                    return next(err);
                }
                if (!product) {
                    return next({
                        msg: 'Product Not Found',
                        status: 404
                    })
                }
                if (req.file) {
                    req.body.images = [req.file.filename];
                }
                req.body.user = req.loggedInUser._id;
                map_product_req(product, req.body);
                product.save(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                })
            })

    })
    .delete(authenticate, isAdmin, function (req, res, next) {
        ProductModel.findById(req.params.id, function (err, product) {
            if (err) {
                return next(err);
            }
            if (!product) {
                return next({
                    msg: "product not found",
                    status: 404
                })
            }
            product.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(removed);
            })
        })
    });


module.exports = router;

