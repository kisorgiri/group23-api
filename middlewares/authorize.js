module.exports = function (req, res, next) {
    if (req.query.role === 'admin') {
        return next();
    } else {
        res.json({
            msg: "You dont have access",
            status: 403
        })
    }
}