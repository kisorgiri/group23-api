const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    var token;
    if (req.headers['token']) {
        token = req.headers['token'];
    };
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token'];
    }
    if (req.query.token) {
        token = req.query.token;
    }
    if (token) {
        jwt.verify(token, config.jwtSecret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            // console.log('decoded value >>', decoded);
            // if eveything is valid add new property in request to determine the current active user

            UserModel.findById(decoded._id, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    req.loggedInUser = user;
                    return next();
                }
                else {
                    next({
                        msg: "user removed from system",
                        status: 404
                    })
                }
            })
        })

    }
    else {
        next({
            msg: "Token Not Provided",
            status: 400
        })
    }
}