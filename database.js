// nosql database
// collection based approach
// documents
// schema less design
// highly scalable
// data format ==> JSON
// var obj = {
//     name:{
//         nickName:'test',
//         fullName:'kishor giri'
//     },
//     phone:[333,333],
//     address:{
//         temp_addr:['a','b'],
//         permanent_addr:'bkt'
//     }
// }
// mongodb, couchdb, dynamodb,redis


// once mongodb is installed and path is fixed
// you should be able to access the mongo shell from any location via terminal
// mongo command is used to connect to server 
// it will prompts > interface to accepts shell command
// optional mongod ===> before mongo to run the programme

// shell command
// show dbs list all the available database
// use <db_name> if db_name exists select existing db else create new and select it

// db  shows selected db
// show collections ==> shows the list of collection from selected db
// db.<col_name>.insert({valid json object});
// eg. db.users.insert({name:'brodway'});

// db.users.find({Query_builder}) // 
// db.users.find({}) // fetch all the documents of the collections

// CRUD
// create/
// db.<col_name>.insert({valid json})

// read
// db.<col_name>.find({query_builder});
// db.<col_name>.find({}).pretty() // prettify result
// db.<col_name>.find({}).count()
// db.<col_name>.find({}).sort({_id:-1})
// db.<col_name>.find({}).skip(1)
// db.<col_name>.find({}).limit(2)


// update
// db.<col_name>.update({},{},{})
// db.<col_name>.update({query_builder},{$set:{json data to be updated}},{options})
// db.<col_name>.update({brand:'sony'},{$set:{name:'somthing'}},{multi:true,upsert:true});

// delete
// db.<col_name>.remove({query_builder});

// drop collections
// db.<col_name>.drop();

// drop database
// db.dropDatabase();

// MONGOOSE////////////////////////////
// advantagaes
// 1. schema based solution
// 2. data type
// 3. methods
// 4.easy indexing
// 5.middlewares


//###############BACKUP & RESTORE#################
// human redable / machine readable
// csv/json || bson
// backup and restore command
// mongodump mongorestore ==> bson 
// mongoimport mongoexport ===> json/csv
// these commands always came in pair
// these commands can be run from any directory

// backup
// bson format
// command
// mongodump ===> backup all the available database in default dump folder
// mongodump --db <db_name> backup given database in default dump folder
// mongodump --db <db_name> --out <path_to_output_diretory>

// restore
// bson format
// command
// mongorestore ===> it search for dump folder and restore all the database of dump folder
// mongorestore <path_to_source_folder>

// json and csv
// backup
// command
// mongoexport
// json
// mongoexport --db <db_name> --collection <col_name> --out <path_to_destination_folder_with_.json_extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_destination_folder_with_.json_extension>
// import command
// mongoimport --db <db_name> --collection <col_name> <path_to_source_file>

// csv
// backup
// mongoexport --db <db_name> --collection <col_name> --type=csv --fields 'comma_seperated_filed_name_to_be_exported' --out <path_to_destination_folder_with.csv_extension>
// mongoexport --db <db_name> --collection <col_name> --type=csv --query="{key:'value'}" --fields 'comma_seperated_filed_name_to_be_exported' --out <path_to_destination_folder_with.csv_extension>

// restore 
// mongoimport --db <db_name> --collection <collection_name> --type=csv <path_to_source_file> --headerline


//###############BACKUP & RESTORE#################
